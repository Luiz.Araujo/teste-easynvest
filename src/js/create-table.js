const users = JSON.parse(localStorage.getItem('users'));
const table = document.querySelector('.table tbody');
createTable(table, users);

function createTable(table, users) {
  if (users.length == 0) {
    const line = table.insertRow(0);
    line.innerHTML = `<td colspan="5">Não existem dados cadastrados</td>`;
  }
  users.forEach((user, i) => {
    const line = table.insertRow(i + 1);
    const count = Object.entries(user).length;
    Object.keys(user).forEach((key, index) => {
      const cell = line.insertCell(index);
      cell.innerHTML = user[key];

      if (index + 1 === count) {
        const cell = line.insertCell(index + 1);
        cell.innerHTML = `<button class="btn-delete-line" onClick="removeLine(${i})"><img src="./img/delete-icon.svg" alt="Remover" class="user-form__error-icon"></button>`;
      }
    });
    table.appendChild(line);
  });
}

function removeLine(index) {
  const confirmDelete = confirm('Deseja apagar este registro?');
  if (confirmDelete) {
    users.splice(index, 1);
    const table = document.querySelector('.table tbody');
    table.innerHTML = '<tr></tr>';
    createTable(table, users);
    localStorage.setItem('users', JSON.stringify(users));
  }
}
