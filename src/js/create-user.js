const form = document.querySelector('.user-form__form');
const inputs = document.querySelectorAll('.user-form__form-input');

form.addEventListener('submit', e => {
  e.preventDefault();
  const values = [];
  inputs.forEach(input => {
    if (input.value == '' && input.required) {
      input.parentElement.classList.add('error');
    } else {
      input.parentElement.classList.remove('error');
      if (input.name == 'name') {
        if (validateName(input.value)) {
          input.parentElement.classList.remove('error');
          values.push(true);
        } else {
          input.parentElement.classList.add('error');
          values.push(false);
        }
      }
      if (input.name == 'email') {
        if (validateEmail(input.value)) {
          input.parentElement.classList.remove('error');
          values.push(true);
        } else {
          input.parentElement.classList.add('error');
          values.push(false);
        }
      }
      if (input.name == 'cpf') {
        if (validateCPF(input.value)) {
          input.parentElement.classList.remove('error');
          values.push(true);
        } else {
          input.parentElement.classList.add('error');
          values.push(false);
        }
      }
      if (input.name == 'phone') {
        if (validatePhone(input.value)) {
          input.parentElement.classList.remove('error');
          values.push(true);
        } else {
          input.parentElement.classList.add('error');
          values.push(false);
        }
      }
    }
  });
  if (!values.includes(false)) {
    this.loadIcon = document.querySelector('.user-form__btn--hide');
    this.btnText = document.querySelector('.user-form__btn-text');

    this.loadIcon.classList.remove('user-form__btn--hide');
    this.btnText.classList.add('user-form__btn--hide');

    saveLocalStorage();
    document.querySelector('.user-form__form').reset();

    setTimeout(() => {
      this.loadIcon.classList.add('user-form__btn--hide');
      this.btnText.classList.remove('user-form__btn--hide');
    }, 1000);

    window.location.href = 'users.html';
  }
});

const [formInputs, btn] = [
  document.querySelectorAll('[type="text"]'),
  document.querySelector('.user-form__btn'),
];
btn.disabled = true;

for (i = 0; i < formInputs.length; i++) {
  formInputs[i].addEventListener('input', () => {
    const values = [];
    formInputs.forEach(v => values.push(v.value));
    btn.disabled = values.includes('');
  });
}

function saveLocalStorage() {
  userValue = {
    name: document.querySelector('#name').value,
    cpf: document.querySelector('#cpf').value,
    phone: document.querySelector('#phone').value,
    email: document.querySelector('#email').value,
  };
  users = localStorage.getItem('users');

  if (users) {
    users = JSON.parse(users);
    users.push(userValue);
  } else {
    users = [];
    users.push(userValue);
  }

  localStorage.setItem('users', JSON.stringify(users));
}

function validateName(name) {
  const re = /(.*[a-z]){3}/i;
  return re.test(String(name).toLowerCase());
}

function validateEmail(email) {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

function validatePhone(phone) {
  const re = /^(?:(?:\+|00)?(55)\s?)?(?:\(?([1-9][0-9])\)?\s?)?(?:((?:9\d|[2-9])\d{3})\-?(\d{4}))$/;
  return re.test(String(phone).toLowerCase());
}

function validateCPF(inputCPF) {
  let soma = 0;
  let resto;
  var inputCPF = document.getElementById('cpf').value;

  if (inputCPF == '00000000000') return false;
  for (i = 1; i <= 9; i++)
    soma += parseInt(inputCPF.substring(i - 1, i)) * (11 - i);
  resto = (soma * 10) % 11;

  if (resto == 10 || resto == 11) resto = 0;
  if (resto != parseInt(inputCPF.substring(9, 10))) return false;

  soma = 0;
  for (i = 1; i <= 10; i++)
    soma += parseInt(inputCPF.substring(i - 1, i)) * (12 - i);
  resto = (soma * 10) % 11;

  if (resto == 10 || resto == 11) resto = 0;
  if (resto != parseInt(inputCPF.substring(10, 11))) return false;
  return true;
}
