<h1 align="center">
  <img src="./src/img/easynvest-logo.png" alt="Easynvest" width="400" />
</h1>

<p align="center">
  <a href="#projeto">Projeto</a>&nbsp;&nbsp;|&nbsp;&nbsp;
  <a href="#layout">Layout</a>&nbsp;&nbsp;|&nbsp;&nbsp;
  <a href="#como-rodar">Como rodar</a>&nbsp;&nbsp;|&nbsp;&nbsp;
  <a href="#feito-com">Feito com</a>&nbsp;&nbsp;|&nbsp;&nbsp;
  <a href="#licença">Licença</a>&nbsp;&nbsp;|&nbsp;&nbsp;
  <a href="#entre-em-contato">Entre em contato!</a>
</p>

## Projeto

É um simples app que contem duas páginas, uma que exibe um formulário com os campos abaixo, e outra que liste os dados cadastrados.

• Nome completo
• CPF
• Telefone
• Email

Os dados cadastrados são armazenados no localStorage.

## Layout

<img src="./src/img/print.png" alt="Layout" />

## Como rodar

#### Requisitos

Para clonar e rodar a aplicação é necessário:

- [Git](https://git-scm.com)
- [Node](https://nodejs.org/)
- [Yarn](https://yarnpkg.com/)

Na linha de comando:

#### Download

```bash
# Clone o repositório
$ git clone git@gitlab.com:Luiz.Araujo/teste-easynvest.git

# Acesse a pasta do repositório
$ cd teste-easynvest

# Instale as dependências
$ yarn install
```

#### Iniciando

```bash

# Iniciando a aplicação
$ yarn start

# rodando na porta 3000
```

### Testes

Os testes end-to-end foram realizados com o framework Cypress, para visualizar os testes realizados:

```bash
# Iniciar os testes
$ yarn cy:open
```

## Feito com

- [JavaScript](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript) - JavaScript é uma linguagem de programação interpretada estruturada, de script em alto nível com tipagem dinâmica fraca e multiparadigma.
- [Cypress](https://www.cypress.io/) - Framework de testes automatizados end-to-end usando JavaScript

## Licença

Este projeto foi feito sob a licença MIT. Veja a [LICENÇA](https://gitlab.com/Luiz.Araujo/teste-easynvest/-/blob/master/LICENSE) para detalhes.

## Entre em contato!

<a href="https://www.linkedin.com/in/luiz-araujojr/" target="_blank" >
  <img alt="Linkedin - Luiz Araújo" src="https://img.shields.io/badge/Linkedin--%23F8952D?style=social&logo=linkedin">
</a>&nbsp;&nbsp;&nbsp;
<a href="mailto:luizcaj@yahoo.com.br" target="_blank" >
  <img alt="Email - Luiz Araújo" src="https://img.shields.io/badge/Email--%23F8952D?style=social&logo=yahoo!">
</a>

---

Feito com ❤️ por Luiz Araújo.
