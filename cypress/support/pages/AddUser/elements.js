export const ELEMENTS = {
  name: '#name',
  email: '#email',
  cpf: '#cpf',
  phone: '#phone',
  submit: '.user-form__btn'
}
