const el = require('./elements').ELEMENTS;

class AddUser {
  accessAddUserPage(){
    cy.visit('http://localhost:3000/');
  }

  sendAddUserForm(){
    cy.get(el.name).type('Luiz Araújo');
    cy.get(el.email).type('luiz@mail.com');
    cy.get(el.cpf).type('12345678909');
    cy.get(el.phone).type('11 999999999');

    cy.get(el.submit).click().should(() => {
      expect(localStorage.getItem('users')).to.eq('[{"name":"Luiz Araújo","cpf":"12345678909","phone":"11 999999999","email":"luiz@mail.com"}]')
    });
  }
}

export default new AddUser();
