const el = require('./elements').ELEMENTS;

class ListUsers {
  accessListUsersPage(){
    cy.visit('http://localhost:3000/users.html', {
      onBeforeLoad: (win) => {
          win.localStorage.clear();
          win.localStorage.setItem('users', `
          [
            {"name":"Luiz","cpf":"12345678909","phone":"11 987654321","email":"luiz@mail.com"},
            {"name":"Teste","cpf":"12345678909","phone":"11 987654321","email":"teste@mail.com"}
          ]
          `);
      }
    });
  }

  deleteUsers(){
    cy.get(el.deleteButton).click();
    cy.on('window:confirm', () => true);
    cy.get(el.line).should(($lines) => {
      expect($lines).to.have.length(1)
    })
    cy.get(el.deleteButton).click();
    cy.on('window:confirm', () => true);
    cy.get(el.column).contains('Não existem dados cadastrados')
  }

  buttonGoToAddUserPage(){
    cy.get(el.buttonGoToAddUserPage).click();
  }

  accessAddUserPage(){
    cy.visit('http://localhost:3000/');
  }
}

export default new ListUsers();
