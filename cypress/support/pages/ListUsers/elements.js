export const ELEMENTS = {
  deleteButton: '.btn-delete-line:last',
  line: 'thead > tr',
  column: 'td',
  buttonGoToAddUserPage: '.header__link'
}
