/// <reference types="cypress" />

import AddUser from '../support/pages/AddUser';

describe('AddUser', () => {
    it('should create new user', () => {
      AddUser.accessAddUserPage();
      AddUser.sendAddUserForm();
    });
});
