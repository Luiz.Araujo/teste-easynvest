/// <reference types="cypress" />

import ListUsers from '../support/pages/ListUsers';

describe('AddUser', () => {
    it('should delete an user', () => {
      ListUsers.accessListUsersPage();
      ListUsers.deleteUsers();
    });

    it('should access add user page', () => {
      ListUsers.buttonGoToAddUserPage();
      ListUsers.accessAddUserPage();
    });
});
